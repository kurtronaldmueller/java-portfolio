/*
 * Project Description:
 * This was a class project that is an example of recursion. This program allows
 * the user to enter in a list of integers and a desired sum. The program then 
 * recursively finds all the subsets of numbers that add up to the desired sum.
 */
package com.kurtronaldmueller.subset;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class SubsetDriver {

    public static void main( String[] args ) {
        
        /*
         * DECLARE VARIABLES 
         */

        Scanner keyboard = new Scanner( System.in );
        List<Double> numbers = new ArrayList<Double>();

        double number = 0;
        boolean enteringNumbers = true;
        boolean invalidSum = true;

        
        /* 
         * PRINT OUT WELCOME MESSAGE
         */
        
        System.out.println( "Welcome. This program allows you to enter in a list of numbers and a" );
        System.out.println( "desired sum. It then prints out all subsets of numbers within that list" );
        System.out.println( "that equal that sum.\n" );
        
        
        /* 
         * GET USER INPUT 
         */
        
        System.out.println( "First, enter in all #s in the list. When you're done entering numbers, " );
        System.out.println( "enter in anything that's not a number. Please only enter one # of a time." );
        
        // while the user is entering numbers
        while( enteringNumbers ) {
        	
        	// get user input
            try {
                System.out.print( "Enter #: " );
                number = keyboard.nextDouble();
                numbers.add( number );
            }
            // the user did not enter a number, signaling they are done entering in numbers to the 
            // number list
            catch( InputMismatchException e ) {
                System.out.println( "Terminated entering numbers into the list." );
                keyboard.nextLine();
                enteringNumbers = false; // stop the loop
            }
        }

        System.out.println();

        // get the desired sum
        do {
        	// get the desired sum from teh user
            try {
                System.out.print("Enter the sum to find: ");
                number = keyboard.nextDouble();
                invalidSum = false;
            }
            // the user has not entered a valid number
            catch( InputMismatchException e ) {
                System.out.println("Enter in a valid number.");
                keyboard.nextLine();
            }            
        } while( invalidSum );

        // convert the list to an array of doubles
        Double[] numberList = numbers.toArray( new Double[ numbers.size() ] );

        // find all the valid subsets of the desired sum
        System.out.println( "\nValid subsets:" );
        findSubset( numberList, number ); 
    }

    /**
     * Wrapper method for the findSubset recursive method. User calls this method with a numbers list
     * and a desired sum. This method will then call the actual recursive method.
     * 
     * @param numberList The list of numbers that will be used to find the desired sum.
     * @param desiredSum The desired sum to be found.
     */
    public static void findSubset( Double[] numberList, double desiredSum ) {
        findSubset( numberList, desiredSum, 0, 0, "" );
    }

    /**
     * A recursive method that finds all subsets of numbers within the list that equals the desired sum.
     * 
     * @param numberList The list of numbers that will be used to find the desired sum.
     * @param desiredSum The desired sum to be found.
     * @param index Accesses the current element in the numbers list.
     * @param runningSum The running sum used to compare the current sum with the desired sum.
     * @param currentSubset Outputs the current subset of numbers that adds up to the desired sum.
     */
    public static void findSubset( Double[] numberList, double desiredSum, int index, double runningSum, String currentSubset ) {

        // if the index has come the end of the number's list
        if( ( index == numberList.length ) ) {

            // check to see if the current sum equals the desired sum
            if( runningSum == desiredSum ) {
                
                // if it does, and the current subset's length is greater than 1, output the subset to the screen 
                try {
                    System.out.println( "{" + currentSubset.substring( 0, currentSubset.length()-1 ) + " } = " + runningSum );                     
                }
                catch( StringIndexOutOfBoundsException e ) {
                    // do nothing
                }
            }
            return; // return back to the previous call
        }

        // include numbers[index]
        findSubset( numberList, desiredSum, index + 1, runningSum + numberList[index], currentSubset + " " + numberList[index] + "," );

        // exclude numbers[index]
        findSubset( numberList, desiredSum, index + 1, runningSum, currentSubset );
    }
}