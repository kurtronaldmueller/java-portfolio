/* 
 * Chapter   : 5
 * Project   : 7
 * File Name : NegativeNumberException.java
 * @author   : Kurt Mueller
 * Date      : 2012/03/26
 * 
 * Classes needed and Purpose (Input, Processing, Output)
 * main class - DispenseChangeDriver
 * processing - ChangeCombo, Coin, NegativeNumberException 
*/
package com.kurtronaldmueller.changecombos;

/**
 * This exception class is used to validate that numbers are positive.
 * 
 * @author KurtRonaldMueller Amazingly intelligent.
 */
@SuppressWarnings("serial")
public class NegativeNumberException extends Exception {
    
    /**
     * Thrown when the number is not a positive number.
     */
    public NegativeNumberException() {
        super( "Negative number. Number must be positive." );
    }
    
    public NegativeNumberException( String message ) {
        super( message );
    }
}