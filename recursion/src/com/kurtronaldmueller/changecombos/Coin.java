/* 
 * Chapter   : 5
 * Project   : 7
 * File Name : Coin.java
 * @author   : Kurt Mueller
 * Date      : 2012/03/26
 * 
 * Classes needed and Purpose (Input, Processing, Output)
 * main class - DispenseChangeDriver
 * processing - ChangeCombo, Coin, NegativeNumberException 
*/
package com.kurtronaldmueller.changecombos;

/**
 * Coin class. Used to store coins found in American currency ( i.e. penny, nickel,
 * dime, quarter, half-dollar, & dollar coins ).
 * @author KurtRonaldMueller
 */
public class Coin {
    
    private String name; // the name of the coin
    private int value;   // the coin's value
    private int weight;  // the "weight" of the coin - used to identify unique combinations of coins
    
    /**
     * Coin constructor used to create coins found in the American currency.
     * 
     * @param name The coin's name.
     * @param value The coin's value.
     * @param weight The coin's weight.
     */
    public Coin( String name, int value, int weight ) {
        this.name   = name;
        this.value  = value;
        this.weight = weight;
    }
    
    /**
     * The copy constructor.
     * 
     * @param coin The coin to copy.
     */
    public Coin( Coin coin ) {
        this.name   = coin.getName();
        this.value  = coin.getValue();
        this.weight = coin.getWeight();
    }

    /* SETTERS */
    public void setName( String name )  { this.name   = name;   }
    public void setValue( int value )   { this.value  = value;  }
    public void setWeight( int weight ) { this.weight = weight; }
    
    /* GETTERS  */
    public String getName() { return name;   }
    public int getValue()   { return value;  }
    public int getWeight()  { return weight; }    
}