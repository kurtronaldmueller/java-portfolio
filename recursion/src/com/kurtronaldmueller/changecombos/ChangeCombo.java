/* 
 * Chapter   : 5
 * Project   : 7
 * File Name : ChangeCombo.java
 * @author   : Kurt Mueller
 * Date      : 2012/03/26
 * 
 * Classes needed and Purpose (Input, Processing, Output)
 * main class - DispenseChangeDriver
 * processing - ChangeCombo, Coin, NegativeNumberException 
*/
package com.kurtronaldmueller.changecombos;

import java.util.ArrayList;

/**
 * Holds a change combination. That equals a certain amount.
 * 
 * @author KurtRonaldMueller Incredibly handsome.
 */
public class ChangeCombo {
    
    // the list of coins in the change combination
    private ArrayList<Coin> coins = new ArrayList<Coin>();
    
    /**
     * Creates a new change combination starting with one coin.
     * 
     * @param coin The first coin to add to the change combination.
     */
    public ChangeCombo( Coin coin ) {
        coins.add( new Coin( coin ) );
    }
    
    /**
     * Copy constructor. Used to copy a change combination.
     * 
     * @param changeCombo The change combination to copy.
     */
    public ChangeCombo( ArrayList<Coin> changeCombo ) {
      
        // iterate through each coin in changeCombo and copy it this' change combination
        for( int coinIndex = 0; coinIndex < changeCombo.size(); coinIndex++ ) {
            coins.add( changeCombo.get( coinIndex ) );
        }
    }
    
    /**
     * Add a coin to the change combination.
     * 
     * @param coin The coin to add to the combination.
     */
    public void addCoin( Coin coin ) {
        coins.add( new Coin( coin ) );
    }
    
    /**
     * Gets the change combinations total weight. This is used to compare change combinations
     * against each other. The same total value and total weight means the combination is the 
     * same.
     * 
     * @return The change combination's total weight.
     */
    public int getTotalWeight() {
        int totalWeight = 0;
        
        // iterate through all the coins in the change combination and 
        // tally up the total weight
        for( int coinIndex = 0; coinIndex < coins.size(); coinIndex++ ) {
            totalWeight += coins.get( coinIndex ).getWeight();
        }
        
        return totalWeight;
    }
    
    /**
     * Get the change combination's total value.
     * 
     * @return The change combination's total value.
     */
    public int getTotalValue() {
        int totalValue = 0;
        
        // iterate through all coins in the change combination and tally up the
        // combination's total value
        for( int coinIndex = 0; coinIndex < coins.size(); coinIndex++ ) {
            totalValue += coins.get( coinIndex ).getValue();
        }
        
        return totalValue;
    }
    

    /**
     * Store all change in the current combination to the string.
     * 
     * @return A string of all the coins in the current combination.
     */
    public String toString() {
        
        StringBuilder changeCombination = new StringBuilder();
        
        // iterate through all coins in the change combination and each to a 
        // string
        for( int coinIndex = 0; coinIndex < coins.size(); coinIndex++ ) {
            
            // append the current coin to the current combination string
            changeCombination.append( coins.get( coinIndex ).getValue() );
            
            if( coinIndex < coins.size()-1 ) {
                changeCombination.append( ", " );                
            }
        }
        
        return changeCombination.toString();
    }
}
