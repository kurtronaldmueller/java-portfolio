/**
 * The mismatching symbols exception is thrown when the expression being checked
 * does not have matching opening & closing brackets.
 * 
 * @author kurtronaldmueller
 */
package com.kurtronaldmueller.expressionManager;

@SuppressWarnings("serial")
public class MismatchingSymbolsException extends Exception {

    public MismatchingSymbolsException()
    {
        super( "The symbols do not match & the expression is not balanced." );
    }
    
    public MismatchingSymbolsException( String message )
    {
        super( message );
    }
}
