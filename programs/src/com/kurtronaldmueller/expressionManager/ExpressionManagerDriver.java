/*
 * This is a program that converts mathematical expressions from postfix to infix
 * and vice versa.
 */
package com.kurtronaldmueller.expressionManager;

import java.util.Scanner;

public class ExpressionManagerDriver
{
    public static void main( String[] args )
    {        
        ExpressionManager expressionManager = new ExpressionManager();  // create a new ExpressionManager to handle the expression
        Scanner keyboard                    = new Scanner( System.in ); // handles the user input

        boolean valid = true; // controls the menu loop
        int choice    = 0;    // stores the user menu choice
        String expr;          // stores the expression the user enters
        
        // print welcome message
        System.out.println("Welcome. This is an expression manager."  +
        		           "\nPlease choose from the following menu.");
        
        // while the user has chosen not to quit 
        while( valid )
        {
            // present the user menu
            System.out.println(
            "\n1\tConvert an infix expression to postfix expression."   +
            "\n2\tConvert a postfix expression to an infix expression." +
            "\n3\tEvaluate a postfix expression."                       +
            "\n4\tQuit the program." );
            
            // get user input
            System.out.print( "\nEnter choice: " );
            
            // attempt to get the user input
            // if the input is not a number, throw an exception
            try
            {
                choice = keyboard.nextInt();
                keyboard.nextLine();
            }
            // catch invalid input
            catch( NumberFormatException e ) // if the user doesn't enter in a menu
            {
                System.out.println( "Invalid choice." );
            }
            
            // convert an infix expression to a postfix expression
            if( choice == 1 ) {      
                System.out.print( "\nEnter expression: " );
                expr = keyboard.nextLine();
                
                System.out.println( "Postfix expression: " + expressionManager.convertToPostfix( expr ) );
            }
            // convert a postfix expression to an infix expression
            else if( choice == 2 ) { 
                System.out.print( "\nEnter expression: " );
                expr = keyboard.nextLine();
                
                System.out.println( "Infix expression: " + expressionManager.convertToInfix( expr ) );
            }
            // evaluate a postfix expression
            else if( choice == 3 ) { 
                System.out.print( "\nEnter expression: " );
                expr = keyboard.nextLine();
                
                System.out.println( "Postfix expression = " + expressionManager.evalPostfixExpression( expr ) );
            }
            // exit the program
            else if( choice == 4 ) { 
                System.out.println( "Exiting program." );
                valid = false; // set the menu loop to false to exit the program
            }
            // let the user know an invalid choice was entered
            else {
                System.out.println( "Invalid choice." );
            }   
        }
    }
}