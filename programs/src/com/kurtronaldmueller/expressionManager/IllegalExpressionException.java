/**
 * The illegal expression exception is thrown when the expression being checked
 * cannot be a legal expression.  It is thrown when the number of numbers - the 
 * number of operators != 1. 
 * 
 * @author kurtronaldmueller
 */
package com.kurtronaldmueller.expressionManager;

@SuppressWarnings("serial")
public class IllegalExpressionException extends Exception
{
    public IllegalExpressionException() {
        super( "This is an illegal expression." );
    }
    
    public IllegalExpressionException( String message ) {
        super( message );
    }
}
