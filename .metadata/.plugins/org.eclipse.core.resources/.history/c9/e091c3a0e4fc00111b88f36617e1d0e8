/*
 * This is an (uncompleted) game that I created for a class project. It is a game
 * called "ClickIt!" in which the goal is to remove all the pieces from the board by
 * clicking on pieces that have 2-3 adjacent cells of the same color.
 * 
 * The main game is completed but scoring and game preferences has yet to be implemented.
 * 
 * The game makes extensive use of recursive backtracking in order to determine which
 * pieces can be removed from the board.
 */
package com.kurtronaldmueller.clickit;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.Stack;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

@SuppressWarnings("serial")
public class ClickIt extends JFrame {

    /*
     * GUI Elements
     */

    /** The game board panel */
    private JPanel gameBoardPanel;
    /** the menu bar used to access each operation */
    private JMenuBar menuBar;
    /** the game menu option */
    private JMenu gameMenu;
    /** the menu item that starts a new game */
    private JMenuItem newGameItem;
    /** the preferences menu item */
    private JMenuItem prefItem;
    /** the exit menu item */
    private JMenuItem exitItem;


    /*
     * The Gameboard
     */

    /** array of JButtons that serves as the game board GUI*/
    private JButton[][] boardGUI;
    /** the board that actually handles the game logic */
    private int[][] board;

    /** The list of colors to utilize */
    private Color colors[];
    /** Color used to indicate that the piece is empty */
    private static Color DARK_GREY;
    /** # used to indicate that the piece is empty*/
    private static int EMPTY_PIECE = -1;

    /*
     * Initial Settings
     */
    /** initial # of columns */
    private static int NUMBER_OF_COLUMNS = 5;
    /** initial # of rows */
    private static int NUMBER_OF_ROWS = 10;
    /** initial # of colors */ 
    private static int NUMBER_OF_COLORS = 5; 
    /** initial window width */
    private static int WINDOW_WIDTH  = NUMBER_OF_COLUMNS * 39;
    /** initial window height */
    private static int WINDOW_HEIGHT = NUMBER_OF_ROWS * 29 + 45;

    // strings used for the label of each menu item
    private static final String GAME_MENU_STRING   = "Game Menu";
    private static final String NEW_GAME_STRING    = "New Game";
    private static final String PREFERENCES_STRING = "Preferences";
    private static final String EXIT_GAME_STRING   = "Exit";

    // the main app frame
    static ClickIt mainFrame;
    
    /** a stack data structure that holds the pieces to be removed */
    static Stack<Piece> piecesToRemove;
    
    /** */
    static int blockSize = 0;
    static int MINIMUM_BLOCK_SIZE = 3;
    
    public static void main( String[] args ) {

        // set the dark grey color to it's value
        DARK_GREY = new Color( 50, 50, 50 );
        // initialize pieces to remove stack
        piecesToRemove = new Stack<Piece>();
        

        mainFrame = new ClickIt();
        mainFrame.setTitle( "Click It!" );
        mainFrame.setLocationRelativeTo( null );
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize( WINDOW_WIDTH, WINDOW_HEIGHT );
        mainFrame.setResizable(false);
        mainFrame.setVisible( true );
    }

    /**
     * Default constructor that loads up the application.
     */
    public ClickIt() {
        this.startNewGame();
    }

    /**
     * Resets the game board and starts a new game.
     */
    public void startNewGame() {
        
        // clear the pieces to remove stack
        piecesToRemove.clear();
        
        /*
         * CREATE MENU BAR
         */

        // create the menu bar
        menuBar = new JMenuBar();

        // create the game menu
        gameMenu = new JMenu( GAME_MENU_STRING );
        menuBar.add( gameMenu );

        // create the new game menu item
        newGameItem = new JMenuItem( NEW_GAME_STRING );
        newGameItem.addActionListener( new MenuActionListener() );
        gameMenu.add( newGameItem );

        // create the preferences menu item
        prefItem = new JMenuItem( PREFERENCES_STRING );
        prefItem.addActionListener( new MenuActionListener() );
        gameMenu.add( prefItem );

        // add a separator
        gameMenu.add( new JSeparator() );

        // create the exit menu item
        exitItem = new JMenuItem( EXIT_GAME_STRING );
        exitItem.addActionListener( new MenuActionListener() );
        gameMenu.add( exitItem );

        // add the menu bar to the app
        this.setJMenuBar( menuBar );

        /*
         * CREATE GAME BOARD
         */

        gameBoardPanel = new JPanel( new GridLayout( NUMBER_OF_ROWS, NUMBER_OF_COLUMNS, 0, 0 ) );
        boardGUI = new JButton[NUMBER_OF_ROWS][NUMBER_OF_COLUMNS];
        board = new int[NUMBER_OF_ROWS][NUMBER_OF_COLUMNS];

        /*
         * PROPOGATE GAME BOARD PIECES
         */
        
        // random # generator used to generate random colors
        Random randGen = new Random();

        // create a list of colors
        colors = new Color[NUMBER_OF_COLORS];

        // generate the colors list 
        for( int colorIndex = 0; colorIndex < colors.length; colorIndex++ ) {
            colors[colorIndex] = new Color( randGen.nextInt( 255 ) + 1, randGen.nextInt( 255 ) + 1, randGen.nextInt( 255 ) + 1 );
        }

        // stores the index of the current color to use
        int nextColorIndex = 0;

        // fill game board pieces with the chosen colors
        for( int row = 0; row < NUMBER_OF_ROWS; row++ ) {
            for( int col = 0; col < NUMBER_OF_COLUMNS; col++ ) {

                // create a new button and color it a color from the colors list
                boardGUI[row][col] = new JButton();
                boardGUI[row][col].setContentAreaFilled( true );
                boardGUI[row][col].setOpaque( true );
                boardGUI[row][col].setBorderPainted( false );

                // generate the next integer that references a color from the color list
                nextColorIndex = randGen.nextInt( NUMBER_OF_COLORS );

                // set the background color to a color from the color list
                boardGUI[row][col].setBackground( colors[nextColorIndex] );
                // store the color index in the board data struct 
                board[row][col] = nextColorIndex;

                // add an action listener
                boardGUI[row][col].addActionListener( new GameBoardButtonListener( row, col ) );
                gameBoardPanel.add( boardGUI[row][col] );
            }
        }

        // add the game board panel to the window
        this.add( gameBoardPanel, BorderLayout.NORTH );
    }

    /**
     * Action listener for the menu bar commands.
     */
    private class MenuActionListener implements ActionListener {

        @Override
        public void actionPerformed( ActionEvent ae ) {

            String action = ae.getActionCommand();

            // start a new game
            if( action.equalsIgnoreCase( NEW_GAME_STRING ) ) {
                // destroy the current frame
                mainFrame.dispose();
                
                // create a new one
                main(null);
            }
            // go to the preferences submenu
            else if( action.equalsIgnoreCase( PREFERENCES_STRING ) ) {
                openPrefMenu();
            }
            // exit the program
            else if( action.equalsIgnoreCase( EXIT_GAME_STRING ) ) {
                System.exit(0);
            }

        }
    }
    
    /**
     * Opens the prefences menu... currently doesn't work.
     */
    private void openPrefMenu() {
        JPanel prefPanel = new JPanel( new GridLayout( 2, 3, 5, 5 ) );
        
        JDialog prefDialog = new JDialog( );
        prefDialog.setTitle( "Preferences" );
        prefDialog.setLocationRelativeTo( this );
        prefDialog.setSize( 300, 300 );
        prefDialog.setVisible( true );
        
        JLabel rowLabel   = new JLabel( "# of rows:" );
        JLabel colLabel   = new JLabel( "# of columns:" );
        JLabel colorLabel = new JLabel( "# of colors:");
        
        SpinnerNumberModel rowSpinnerModel   = new SpinnerNumberModel( NUMBER_OF_ROWS,    0, 50, 1 );
        SpinnerNumberModel colSpinnerModel   = new SpinnerNumberModel( NUMBER_OF_COLUMNS, 0, 50, 1 );
        SpinnerNumberModel colorSpinnerModel = new SpinnerNumberModel( NUMBER_OF_COLORS,  0, 50, 1 );
        
        JSpinner rowSpinner = new JSpinner( rowSpinnerModel );
        JSpinner colSpinner = new JSpinner( colSpinnerModel );
        JSpinner colorSpinner = new JSpinner( colorSpinnerModel );
        
        NUMBER_OF_ROWS    = rowSpinnerModel.getNumber().intValue();
        NUMBER_OF_COLUMNS = colSpinnerModel.getNumber().intValue();
        NUMBER_OF_COLORS  = colorSpinnerModel.getNumber().intValue();
        
        prefPanel.add( rowLabel );
        prefPanel.add( rowSpinner );
        prefPanel.add( colLabel );
        prefPanel.add( colSpinner );
        prefPanel.add( colorLabel );
        prefPanel.add( colorSpinner );
        
        JLabel prefLabel = new JLabel( "Preferences" );
        prefDialog.add( prefLabel, BorderLayout.NORTH );
        prefDialog.add( prefPanel, BorderLayout.CENTER );
    }

    /**
     * Action listener used for the game board pieces.
     */
    private class GameBoardButtonListener implements ActionListener {

        /** the row the button is in */
        private int row;
        /** the column the button is in */
        private int col;

        /**
         * Stores the coordinates of the specific JButton
         * 
         * @param row The row of the piece that was pressed.
         * @param col The pice of the button that was pressed.
         */
        public GameBoardButtonListener( int row, int col ) {
            this.row = row;
            this.col = col;
        }

        /**
         * Remove pieces from the board.
         */
        public void actionPerformed( ActionEvent e ) {

            if( board[row][col] != EMPTY_PIECE ) {
                blockSize = 0;

                // mark the pieces to remove
                markPieces( row, col, board[row][col] );
                
                // remove the pieces
                removePieces();
                
                // shift pieces down
                shiftDown();
                
                // shift pieces left
                try {
                    shiftLeft();
                }
                // this err should never ben thrown
                catch( StackOverflowError err ) {
                    
                }
            }
        }
    }

    
    /**
     * A recursive method that marks pieces from the board.
     * 
     * @param row The current row.
     * @param col The current column.
     * @param color The current color.
     * @postcondition The 'removePieces' will be ran.
     */
    public boolean markPieces( int row, int col, int color ) {

        // the algorithm has reached the one of the edges of the board
        if( row < 0 || col < 0 || row >= boardGUI.length || col >= boardGUI[0].length ) {
            return false; // cell is out of bounds
        }
        // the current board piece does not match the color to be removed
        else if( color != board[row][col] ) {
            return false;
        }
        // match found, remove the pieces
        else if( color == board[row][col] ) {
            blockSize++;
            piecesToRemove.add( new Piece( row, col, color ) );
            board[row][col] = EMPTY_PIECE;
        }

        // go to another piece on the board and repeat the above process
        if( markPieces( row - 1, col, color ) ||
            markPieces( row + 1, col, color ) ||
            markPieces( row, col - 1, color ) ||
            markPieces( row, col + 1, color ) ) {
            
            blockSize++;
            piecesToRemove.add( new Piece( row, col, color ) );
            board[row][col] = EMPTY_PIECE;
            
            return true;
        }
        else {
            return false;   
        }
    }
    
    /**
     * Removes pieces from the board. If the # of pieces is less than 2, the game
     * will not remove the pieces but rather, put them back into place.
     * 
     * @precondition The 'markPieces' method has ran. 
     */
    public void removePieces() {        
        Piece currentPiece = new Piece( );
        int currRow;
        int currCol;
        int currColor;
        
        // only if the # of pieces to remove is greater than 2
        if( piecesToRemove.size() >= 2 ) {
            
            while( ! piecesToRemove.empty() ) {
                currentPiece = new Piece( piecesToRemove.pop() );
                currRow = currentPiece.getRow();
                currCol = currentPiece.getCol();
                
                board[currRow][currCol] = EMPTY_PIECE;
                boardGUI[currRow][currCol].setBackground( DARK_GREY );
            }
        }
        // otherwise, restore the original color
        else {
            while( ! piecesToRemove.empty() ) {
                currentPiece = new Piece( piecesToRemove.pop() );
                currRow = currentPiece.getRow();
                currCol = currentPiece.getCol();
                currColor = currentPiece.getColor();
                
                board[currRow][currCol] = currColor;
            }
        }

        piecesToRemove.clear();
    }

    /**
     * After a pieces have been removed from the game board. Shift all pieces down
     * until they reach the board edge or a piece with another color.
     * 
     * @precondition A piece has been removed.
     */
    public void shiftDown( ) {

        boolean shiftingDown; // used to mark when the shifting down process is taking place
        int currentRow;       // the current row on the game board
        int currentColor;

        // start the algorithm at the the bottom left corner of the board and work
        // from left to right and then upward & repeat until the upper board edge
        // has been reached
        for( int row = board.length-1; row >= 0; row-- ) {
            for( int col = 0; col < board[0].length; col++ ) {

                currentRow = row;
                currentColor = board[row][col];
                shiftingDown = true;

                // the shifting down process
                while( shiftingDown ) {

                    currentRow++;

                    // the algorithm has either reached the board edge or the piece is
                    // currently being occupied by another color
                    if( currentRow >= board.length || board[currentRow][col] != EMPTY_PIECE ) {
                        shiftingDown = false;
                    }
                    else {
                        board[currentRow-1][col] = EMPTY_PIECE;
                        boardGUI[currentRow-1][col].setBackground( DARK_GREY );
                    }
                }

                // mark the final board piece visited as the current color
                if( currentColor != EMPTY_PIECE ) {
                    board[currentRow-1][col] = currentColor;
                    boardGUI[currentRow-1][col].setBackground( colors[currentColor] );                    
                }
            }
        }
    }

    /**
     * Checks to see if any pieces need to be shifted left. If any of the pieces
     * are shifted left, the method calls itself again to check if the pieces needed
     * to be shifted left again. This is necessary when pieces needed to be shifted
     * left father than two blocks.
     *  
     * @throws StackOverflowError Thrown when a stack overflow is called.
     */
    public void shiftLeft( ) throws StackOverflowError {

        boolean shiftingLeft; // set to true during the shifting left process
        boolean checkLeftAgain = false; // set to true when at least one piece has shifted left
        int currentRow; // the current row being shifted left

        int lastRow = board.length-1;

        // start at the bottom left corner of the board
        for( int col = 0; col < board[0].length; col++ ) {

            // if the current piece in the bottom row of the board is empty & not in the last column
            if( board[lastRow][col] == EMPTY_PIECE && col+1 < board[0].length ) {
                shiftingLeft = true;
                currentRow = lastRow;
                
                // used to switch the variable on only once
                if( ! checkLeftAgain ) {
                    checkLeftAgain = true;
                }

                // the shifting left process
                while( shiftingLeft ) {

                    // the adjacent piece to the right of the current piece is
                    // not empty ( i.e. it currently contains a color )
                    if( currentRow >= 0 && board[currentRow][col+1] != EMPTY_PIECE ) {

                        // copy the color in the adjacent piece to the current piece
                        boardGUI[currentRow][col].setBackground( boardGUI[currentRow][col+1].getBackground() );
                        board[currentRow][col] = board[currentRow][col+1];

                        // mark the adjacent piece as empty 
                        boardGUI[currentRow][col+1].setBackground( DARK_GREY );
                        board[currentRow][col+1] = EMPTY_PIECE;

                        currentRow--;
                    }
                    // the adjacent piece is empty - therefore, the shifting left process is over
                    else {
                        shiftingLeft = false;
                    }
                }
            }
        }
        
        if( checkLeftAgain ) {
            shiftLeft();
        }
    }
}