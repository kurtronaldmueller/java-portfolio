/* Homework No: 4
 * Chapter #:   3
 * Problem #:   4
 * File Name:   ExpressionManagerDriver.java
 * @author:     Kurt Ronald Mueller
 * Date:        March 05, 2012
 * 
 * Classes needed:
 * main class         - ExpressionManagerDriver
 * processing         - ExpressionManager, Operator
 * exception handling - IllegalExpressionException, MismatchingSymbolsException
 */
package edu.miracosta.cs113.hw4.progProj4.expressionManager;

public class Operator
{
    private String name;    // the name of the operator
    private char symbol;    // the symbol of the operator
    private int precedence; // what order the operator will be processed in

    /**
     * Create an operator of type +, -, *, /, or %. 
     * 
     * @param name       The name of the operator (addition, subtraction, multiplication, division, modulus).
     * @param symbol     The operator's symbol (+, -, *, /, %)
     * @param precedence The operator's precedence.
     */
    public Operator( String name, char symbol, int precedence )
    {
        this.name       = name;
        this.symbol     = symbol;
        this.precedence = precedence;
    }

    /**
     * Get the name of the operator.
     * 
     * @return The operator name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set the operator name.
     * 
     * @param name The operator name.
     */
    public void setName( String name ) {
        this.name = name;
    }

    /**
     * Get the symbol of the operator.
     * 
     * @return The operator symbol.
     */
    public char getSymbol() {
        return this.symbol;
    }

    /**
     * Set the symbol of the operator.
     * 
     * @param symbol The operator symbol.
     */
    public void setSymbol( char symbol ) {
        this.symbol = symbol;
    }

    /**
     * Get the importance of the operator.
     * 
     * @return The operator importance.
     */
    public int getPrecedence() {
        return precedence;
    }

    /**
     * Set the importance of the operator.
     * 
     * @param precedence The operator importance.
     */
    public void setPrecedence( int precedence ) {
        this.precedence = precedence;
    }
}