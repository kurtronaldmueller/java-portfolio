/*
 * Project Description:
 * This was a class project that is an example of recursion. This program allows
 * the user to enter an amount of change and then this program will recursively
 * find all change combinations.
 */
package com.kurtronaldmueller.changecombos;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class DispenseChangeDriver {

    // coins used to help sort & store change combinations
    public static Coin penny      = new Coin( "penny",       1, 1 );
    public static Coin nickel     = new Coin( "nickel",      5, 2 );
    public static Coin dime       = new Coin( "dime",       10, 3 );
    public static Coin quarter    = new Coin( "quarter",    25, 4 );
    public static Coin halfDollar = new Coin( "halfdollar", 50, 5 );
    public static Coin dollar     = new Coin( "dollar",    100, 6 );
    
    // the collection of all change combinations that equal the user amount entered
    public static ArrayList<ChangeCombo> changeCombos = new ArrayList<ChangeCombo>();

    public static void main( String[] args ) {

        /* 
         * DECLARE VARIABLES 
         */
        
        Scanner keyboard = new Scanner( System.in ); // collects user input
        boolean invalidInput = true;                 // input validation check
        int amount = 0;                              // the amount of change to find
        
        
        /* 
         * PRINT WELCOME MESSAGE 
         */
        
        System.out.println( "Welcome. This program prints out all change" );
        System.out.println( "combinations for a user-inputted amount.\n");
        
        
        /* 
         * GET USER INPUT 
         */
        
        // while the input is invalid
        do {
            
            try {
                // attempt to get a positive valid integer from the user
                System.out.print( "Enter amount (in cents) : " );
                amount = keyboard.nextInt();
                validatePositiveInteger( amount ); // validate the # is positive
                invalidInput = false; // there was no error, so a valid input was entered
            }
            // if the user input is not a number
            catch( InputMismatchException e ) {
                System.out.println( "Invalid input. Input must be an integer." );
                keyboard.nextLine();
            }
            // if the user inputed number is less than 0
            catch ( NegativeNumberException e ) {
                System.out.println( "Number must be greater than or equal to 0." );
                keyboard.nextLine();
            }
        } while ( invalidInput );
        
        
        /* 
         * FIND ALL CHANGE COMBINATIONS 
         */
        
        System.out.println( "\nAll change combinations for " + amount + " cents:" );
        findChangeCombos( amount );        
        
        
        /* 
         * OUTPUT RESULTS 
         */
        
        outputChangeCombos();
    }
    
    /**
     * Validates if the user inputed number is a positive integer.
     * 
     * @param number The number to validate.
     * @throws NegativeNumberException Thrown when the number is less than 0.
     */
    public static void validatePositiveInteger( Number number ) throws NegativeNumberException {
        
        // if the number is less than 0, throw a negative number exception
        if( number.doubleValue() < 0 ) {
            throw new NegativeNumberException();
        }
        
    }

    /**
     * Wrapper method for the find change combinations recursive method.
     * 
     * @param amountEntered The amount to find all change combinations for.
     */
    public static void findChangeCombos( int amountEntered ) {
        findChangeCombos( amountEntered, amountEntered, 0, new ArrayList<Coin>() );
    }
    
    /**
     * The recursive method that finds a change combination for the amount entered.
     * 
     * @precondition amountEntered is a positive integer
     * @param amountEntered The amount in change to find.
     * @param moneyLeft The amount of change left.
     * @param runningSum The running sum of the current change combination.
     * @param changeCombo A collection of coins that eventually add up to the amount entered.
     */
    public static void findChangeCombos( int amountEntered, int moneyLeft, int runningSum, 
                                         ArrayList<Coin> changeCombo ) {
        
        // if the money left is greater or equal to a dollar coin's value
        if( moneyLeft >= dollar.getValue() ) {
            
            // add the coin to the current change combo
            changeCombo.add( dollar );
            
            // recursively call the method with a new moneyLeft & runningSum amount and
            // an updated change combination
            findChangeCombos( amountEntered,
                              moneyLeft  - dollar.getValue(),
                              runningSum + dollar.getValue(),
                              changeCombo );
            
            // remove the latest coin added on to the change combo; this allows
            // the next if statements to correctly find the change combination for
            // amount entered
            changeCombo.remove( changeCombo.size()-1 );
        }
        
        // if the money left is greater or equal to a half dollar coin
        if( moneyLeft >= halfDollar.getValue() ) {
            
            // add the coin to the current change combo
            changeCombo.add( halfDollar );

            // recursively call the method with a new moneyLeft & runningSum amount and
            // an updated change combination
            findChangeCombos( amountEntered,
                              moneyLeft  - halfDollar.getValue(),
                              runningSum + halfDollar.getValue(),
                              changeCombo );

            // remove the latest coin added on to the change combo; this allows
            // the next if statements to correctly find the change combination for
            // amount entered
            changeCombo.remove( changeCombo.size()-1 );
        }
        
        // if the money left is greater or equal to a quarter coin
        if( moneyLeft >= quarter.getValue() ) {
            
            // add the coin to the current change combo
            changeCombo.add( quarter );

            // recursively call the method with a new moneyLeft & runningSum amount and
            // an updated change combination
            findChangeCombos( amountEntered,
                              moneyLeft  - quarter.getValue(),
                              runningSum + quarter.getValue(),
                              changeCombo );
            
            // remove the latest coin added on to the change combo; this allows
            // the next if statements to correctly find the change combination for
            // amount entered            
            changeCombo.remove( changeCombo.size()-1 );
        }
        
        // if the money left is greater or equal to a quarter coin
        if( moneyLeft >= dime.getValue() ) {
            
            // add the coin to the current change combo
            changeCombo.add( dime );
            
            // recursively call the method with a new moneyLeft & runningSum amount and
            // an updated change combination
            findChangeCombos( amountEntered,
                              moneyLeft  - dime.getValue(),
                              runningSum + dime.getValue(),
                              changeCombo );
            
            // remove the latest coin added on to the change combo; this allows
            // the next if statements to correctly find the change combination for
            // amount entered            
            changeCombo.remove( changeCombo.size()-1 );
        }

        // if the money left is greater or equal to a quarter coin
        if( moneyLeft >= nickel.getValue() ) {
            
            // add the coin to the current change combo
            changeCombo.add( nickel );
            
            // recursively call the method with a new moneyLeft & runningSum amount and
            // an updated change combination
            findChangeCombos( amountEntered,
                              moneyLeft  - nickel.getValue(),
                              runningSum + nickel.getValue(),
                              changeCombo );
            
            // remove the latest coin added on to the change combo; this allows
            // the next if statements to correctly find the change combination for
            // amount entered            
            changeCombo.remove( changeCombo.size()-1 );
        }
        
        // if the money left is greater or equal to a quarter coin
        if( moneyLeft >= penny.getValue() ) {
            
            // add the coin to the current change combo
            changeCombo.add( penny );
            
            // recursively call the method with a new moneyLeft & runningSum amount and
            // an updated change combination
            findChangeCombos( amountEntered,
                              moneyLeft  - penny.getValue(),
                              runningSum + penny.getValue(),
                              changeCombo );
            
            // remove the latest coin added on to the change combo; this allows
            // the next if statements to correctly find the change combination for
            // amount entered            
            changeCombo.remove( changeCombo.size()-1 );
        }
        
        // if the running sum equals the amount of change to find
        if( runningSum == amountEntered ) {
            
            boolean found = false; // used to search through the change combinations
            ChangeCombo tempCombo = new ChangeCombo( changeCombo ); // create a temporary change combo
            
            // compare each change combination's weight with the temporary combination's total weight 
            for( int index = 0; index < changeCombos.size(); index++ ) {
                
                // if the total weights are the same
                if( changeCombos.get( index ).getTotalWeight() == tempCombo.getTotalWeight() ) {
                    
                    found = true; // the change combination has been found
                    break;        // stop the loop
                }
            }
            
            // if the current change combination was not found
            if( ! found ) {
                
                // add the current change combination to the collection of all
                // the change combinations
                changeCombos.add( tempCombo );
            }
        }   
    } // endfindChangeCombos( int amountEntered, int moneyLeft, int runningSum, ArrayList<Coin> changeCombo )
    
    /**
     * Outputs all the change combinations stored in the changeCombos arraylist.
     */
    public static void outputChangeCombos() {
        
        // if the first change combination equals 0, there are no change combinations
        if( changeCombos.get( 0 ).getTotalValue() == 0 ) {
            System.out.println( "No change combinations." );
        }
        // output each change combination for the amount
        else {
            for( int index=0; index < changeCombos.size(); index++ ) {
                System.out.println( ( index+1 ) + ": " + changeCombos.get( index ).toString() );
            }            
        }
    }
}
