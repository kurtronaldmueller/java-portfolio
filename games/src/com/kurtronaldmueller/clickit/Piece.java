package com.kurtronaldmueller.clickit;

/**
 * Stores the current piece.
 */
public class Piece {
    /** the row the current piece resides in */
    private int row;
    /** the column the current piece resides in */
    private int col;
    /** the color the current piece */
    private int color;
    
    /**
     * Default constructor. Should not really be used.
     */
    public Piece() {
        this.row = -1;
        this.col = -1;
        this.color = -1;
    }
    
    /**
     * Create a piece with the specified row, column, and color.
     * 
     * @param row The row of the piece.
     * @param col The column of the piece.
     * @param color The color of the piece.
     */
    public Piece( int row, int col, int color ) {
        this.row = row;
        this.col = col;
        this.color = color;
    }
    
    /**
     * Copy constructor.
     * 
     * @param tempPiece The pieces to construct.
     */
    public Piece( Piece tempPiece ) {
        this.row = tempPiece.getRow();
        this.col = tempPiece.getCol();
        this.color = tempPiece.getColor();
    }

    /*
     * GETTERS 
     */
    public int getRow()   { return row; }
    public int getCol()   { return col;   }
    public int getColor() { return color; }
    
    /* 
     * SETTERS  
     */
    public void setRow( int row ) { this.row = row; }
    public void setCol( int col ) { this.col = col; }
    public void setColor( int color ) { this.color = color; }    
}
